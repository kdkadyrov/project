<label>Title</label>
<input name="title" type="text" id="title">

<label>Text</label>
<input name="text1" type="text" id="text">

<input type="button" value="submit" id="submit" >

<br/>

<div class="row" id="posts"></div>

<?php Yii::app() -> clientScript -> registerCoreScript('jquery', CClientScript::POS_BEGIN);?>

<script>
    $(document).ready(function(){
        $('#submit').click(function(){
            var title = $("#title").val();
            var text = $("#text").val();
            $.ajax({
               // url:'index.php?r=site/saveNewPost&title='+title+'&text='+text,
                url:'<?php echo Yii::app()->request->baseUrl;?>index.php?r=site/saveNewPost',
                type:'POST',
                dataType: 'json',
                data: {
                    title: title,
                    text: text,
                },
                success:function(data){
                    $('#posts').load('<?php echo Yii::app()->request->baseUrl;?>index.php?r=site/table');
                }
            });
        });
        $('#posts').load('<?php echo Yii::app()->request->baseUrl;?>index.php?r=site/table');
    });
</script>