<?php
class SiteController extends Controller{

    public function  actionError(){
        $this->render('error');
    }
    public function actionIndex(){
        $this->render('index');
    }

    public function actionSaveNewPost(){
        if (Yii::app()->request->isAjaxRequest)
        {
            $post = new Posts();
            $post['title'] = Yii::app()->request->getParam('title');
            $post['text'] = Yii::app()->request->getParam('text');
            $post->save();
        }
    }

    public function actionCreate(){
        $this->render('create');
    }

    public function actionTable(){
        $this->layout=false;
        $data = Posts::model() -> findall();
        $this->render('table',array('data'=>$data));
    }
}