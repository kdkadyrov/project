<?php
class Posts extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'posts';
    }

    public function attributeLabels() {
        return array(
            'title' => 'title',
            'text'=>'text',
        );
    }

}
?>
